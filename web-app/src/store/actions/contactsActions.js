import axiosApi from "../../axiosApi";

export const FETCH_CONTACTS_REQUEST = 'FETCH_CONTACTS_REQUEST';
export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS';
export const FETCH_CONTACTS_ERROR = 'FETCH_CONTACTS_ERROR';
export const SEND_CONTACTS_REQUEST = 'SEND_CONTACTS_REQUEST';
export const SEND_CONTACTS_SUCCESS = 'SEND_CONTACTS_SUCCESS';
export const SEND_CONTACTS_ERROR = 'SEND_CONTACTS_ERROR';
export const DELETE_CONTACT_REQUEST = 'DELETE_CONTACT_REQUEST';
export const DELETE_CONTACT_SUCCESS = 'DELETE_CONTACT_SUCCESS';
export const DELETE_CONTACT_ERROR = 'DELETE_CONTACT_ERROR';

export const fetchContactsRequest = () => ({type: FETCH_CONTACTS_REQUEST});
export const fetchContactsSuccess = contacts => ({type: FETCH_CONTACTS_SUCCESS, contacts: contacts});
export const fetchContactsError = error => ({type: FETCH_CONTACTS_ERROR, error: error});
export const sendContactsRequest = () => ({type: SEND_CONTACTS_REQUEST});
export const sendContactsSuccess = () => ({type: SEND_CONTACTS_SUCCESS});
export const sendContactsError = error => ({type: SEND_CONTACTS_ERROR, error: error});
export const deleteContactRequest = () => ({type: DELETE_CONTACT_REQUEST});
export const deleteContactSuccess = id => ({type: DELETE_CONTACT_SUCCESS, id: id});
export const deleteContactError = error => ({type: DELETE_CONTACT_ERROR, error: error});

export const fetchContacts = () => {
    return async dispatch => {
        dispatch(fetchContactsRequest());

        try {
            const response = await axiosApi.get('./contacts.json');
            dispatch(fetchContactsSuccess(response.data));
        } catch (e) {
            dispatch(fetchContactsError(e))
        }
    }
};

export const sendContact = contact => {
    return async dispatch => {
        dispatch(sendContactsRequest());

        try {
            await axiosApi.post(`./contacts.json`, contact);
            dispatch(sendContactsSuccess());
        } catch (e) {
            dispatch(sendContactsError(e));
        }
    }
}

export const updateContact = (id, contact) => {
    return async dispatch => {
        dispatch(sendContactsRequest());

        try {
            await axiosApi.put(`./contacts/${id}.json`, contact);
            dispatch(sendContactsSuccess());
        } catch (e) {
            dispatch(sendContactsError(e));
        }
    }
}

export const deleteContact = id => {
    return async dispatch => {
        dispatch(deleteContactRequest());

        try {
            await axiosApi.delete(`./contacts/${id}.json`);
            dispatch(deleteContactSuccess(id));
        } catch (e) {
            dispatch(deleteContactError(e));
        }
    }
}