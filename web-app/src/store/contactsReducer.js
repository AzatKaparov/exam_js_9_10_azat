import {
    DELETE_CONTACT_ERROR,
    DELETE_CONTACT_REQUEST, DELETE_CONTACT_SUCCESS, FETCH_CONTACTS_ERROR,
    FETCH_CONTACTS_REQUEST,
    FETCH_CONTACTS_SUCCESS, SEND_CONTACTS_ERROR,
    SEND_CONTACTS_REQUEST,
    SEND_CONTACTS_SUCCESS
} from "./actions/contactsActions";

const initialState = {
    contacts: [],
    loading: false,
    error: null,
}

const parseData = obj => {
    return Object.keys(obj).map(key => {
        return {...obj[key], id: key}
    });
};

const contactsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_CONTACTS_REQUEST:
            return {...state, loading: true};
        case FETCH_CONTACTS_REQUEST:
            return {...state, loading: true};
        case DELETE_CONTACT_REQUEST:
            return {...state, loading: true};
        case FETCH_CONTACTS_SUCCESS:
            return {...state, loading: false, contacts: parseData(action.contacts)};
        case SEND_CONTACTS_SUCCESS:
            return {...state, loading: true};
        case DELETE_CONTACT_SUCCESS:
            return {...state, contacts: state.contacts.filter(contact => contact.id !== action.id), loading: false};
        case FETCH_CONTACTS_ERROR:
            return {...state, error: action.error, loading: false};
        case SEND_CONTACTS_ERROR:
            return {...state, error: action.error, loading: false};
        case DELETE_CONTACT_ERROR:
            return {...state, error: action.error, loading: false};
        default:
            return state;
    }
};

export default contactsReducer;