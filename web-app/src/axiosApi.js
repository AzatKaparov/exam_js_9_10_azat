import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://exam-9-a255d-default-rtdb.firebaseio.com/',
})

export default axiosApi;