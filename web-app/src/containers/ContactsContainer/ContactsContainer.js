import React, {useEffect, useState} from 'react';
import './ContactsContainer.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts} from "../../store/actions/contactsActions";
import Preloader from "../../components/UI/Preloader/Preloader";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import ContactItem from "../../components/ContactItem/ContactItem";
import Modal from "../../components/UI/Modal/Modal";
import ContactInfo from "../../components/ContactInfo/ContactInfo";

const ContactsContainer = () => {
    const dispatch = useDispatch();
    const {contacts, loading, error} = useSelector(state => state.contacts);
    const [showModal, setShowModal] = useState(true);
    const [chosenContact, setChosenContact] = useState({});

    const toggleModal = () => {
        setShowModal(!showModal);
    }

    const contactItems = contacts.map(contact => (
        <ContactItem
            name={contact.name}
            key={contact.id}
            avatar={contact.photo}
            chose={() => {
                setChosenContact(contact);
                toggleModal();
            }}
        />
    ));

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch])


    return (
        <>
            <Modal show={showModal} close={toggleModal}>
                {error
                    ? `${error}`
                    : <ContactInfo
                        id={chosenContact.id}
                        name={chosenContact.name}
                        image={chosenContact.photo}
                        phone={chosenContact.phone}
                        email={chosenContact.email}
                    />
                }
            </Modal>
            <Preloader show={loading} />
            <Backdrop show={loading}/>
            <div className="ContactsContainer">
                {contactItems}
            </div>
        </>
    );
};

export default ContactsContainer;