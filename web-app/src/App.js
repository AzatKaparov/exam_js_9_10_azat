import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import ContactsContainer from "./containers/ContactsContainer/ContactsContainer";
import Layout from "./components/UI/Layout/Layout";
import React from "react";
import ContactForm from "./components/ContactForm/ContactForm";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route path="/" exact component={ContactsContainer}/>
            <Route path="/contact/add" exact component={ContactForm}/>
            <Route path="/contact/:id/edit" exact component={ContactForm}/>
            <Route render={() => <h1>Not found</h1>}/>
          </Switch>
        </Layout>
      </BrowserRouter>
    </div>
  );
}

export default App;
