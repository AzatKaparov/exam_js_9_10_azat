import React from 'react';
import './ContactItem.css';

const ContactItem = ({name, avatar, chose}) => {
    return (
        <div className="ContactItem" onClick={chose}>
            <img src={avatar} alt=""/>
            <p>{name}</p>
        </div>
    );
};

export default ContactItem;