import React from 'react';
import {NavLink} from "react-router-dom";
import './Toolbar.css';

const Toolbar = () => {
    return (
        <header className="Toolbar">
            <nav>
                <ul>
                    <li><NavLink to="/">Contacts</NavLink></li>
                </ul>
                <NavLink to="/contact/add" className="add-btn">Add new contact</NavLink>
            </nav>
        </header>
    );
};

export default Toolbar;