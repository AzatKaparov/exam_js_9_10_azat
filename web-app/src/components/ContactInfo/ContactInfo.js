import React from 'react';
import './ContactInfo.css';
import {NavLink} from "react-router-dom";
import {deleteContact} from "../../store/actions/contactsActions";
import {useDispatch} from "react-redux";

const ContactInfo = ({id, name, image, phone, email}) => {
    const dispatch = useDispatch();
    return (
        <div className="ContactInfo">
            <div className="img-block">
                <img src={image} alt={name}/>
            </div>
            <div className="text-block">
                <h3>{name}</h3>
                <p>Phone: {phone}</p>
                <p>Email: {email}</p>
                <NavLink to={`/contact/${id}/edit`}>Edit</NavLink>
                <button onClick={e => {
                    e.preventDefault();
                    dispatch(deleteContact(id));
                }}>Delete</button>
            </div>
        </div>
    );
};

export default ContactInfo;