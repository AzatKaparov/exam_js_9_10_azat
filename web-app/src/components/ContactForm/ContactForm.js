import React, {useEffect, useState} from 'react';
import './ContactForm.css';
import noAvatar from '../../assets/no-avatar.png';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {sendContact, updateContact} from "../../store/actions/contactsActions";
import axiosApi from "../../axiosApi";
import Preloader from "../UI/Preloader/Preloader";
import Backdrop from "../UI/Backdrop/Backdrop";

const ContactForm = ({history, match}) => {
    const dispatch = useDispatch();
    const {loading} = useSelector(state => state.contacts);
    const [selected, setSelected] = useState({
        name: '',
        phone: '',
        email: '',
        photo: '',
    });

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get(`/contacts/${match.params.id}.json`);
            setSelected(response.data);
        }
        if (match.params.id) {
            fetchData().catch(e => console.error(e));
        }
    }, [match.params.id]);

    const onFormSubmit = async e => {
        e.preventDefault();
        const contact = {
            ...selected,
        };
        if (match.params.id) {
            try {
                await dispatch(updateContact(match.params.id, contact));
                history.push('/');
            } catch (e) {
                console.error(e);
            }
        } else {
            try {
                await dispatch(sendContact(contact));
                history.push('/');
            } catch (e) {
                console.error(e);
            }
        }
    };

    const dataChanged = e => {
        const name = e.target.name;
        const value = e.target.value;

        setSelected(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    return (
        <>
            <Preloader show={loading} />
            <Backdrop show={loading}/>
            <div className="container">
            <h1 className="form-title">Add new contact</h1>
            <form onSubmit={onFormSubmit} action="" className="ContactForm">
                <label htmlFor="contactName">Name: </label>
                <input onChange={dataChanged} value={selected.name} placeholder="Enter name" type="text" name="name" id="contactName"/>
                <label htmlFor="contactPhone">Phone: </label>
                <input onChange={dataChanged} value={selected.phone} placeholder="Enter phone" type="phone" name="phone" id="contactPhone"/>
                <label htmlFor="contactEmail">Email: </label>
                <input onChange={dataChanged} value={selected.email} placeholder="Enter email" type="email" name="email" id="contactEmail"/>
                <label htmlFor="contactPhoto">Photo: </label>
                <input onChange={dataChanged} value={selected.photo} placeholder="Enter avatar's url" type="text" name="photo" id="contactPhoto"/>
                <img id="photoPreview" src={selected.photo !== "" ? selected.photo : noAvatar} alt="Avatar preview"/>
                <div className="bottom-block">
                    <button type="submit">Save</button>
                    <NavLink to="/" >Back to contacts</NavLink>
                </div>
            </form>
        </div>
        </>
    );
};

export default ContactForm;