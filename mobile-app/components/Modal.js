import React from 'react';
import {View, StyleSheet, Image, Linking} from "react-native";
import {Button, Overlay, Text} from "react-native-elements";
import {useSelector} from "react-redux";

const Modal = ({toggleOverlay, visible}) => {
    const exact = useSelector(state => state.exact);

    return (
        <View>
            <Overlay
                isVisible={visible}
                onBackdropPress={toggleOverlay}
            >
                <View style={styles.modal}>
                    <Text h4>{exact.name}</Text>
                    <Image
                        source={{uri: exact.photo}}
                        style={{width: 100, height: 100}}
                    />
                    <Text
                        style={styles.link}
                        onPress={() => Linking.openURL(`tel:${exact.phone}`)}>
                        {exact.phone}
                    </Text>
                    <Text
                        style={styles.link}
                        onPress={() => Linking.openURL(`mailto:${exact.email}`)}>
                        {exact.email}
                    </Text>
                    <Button onPress={toggleOverlay} title="Back to contacts"/>
                </View>
            </Overlay>
        </View>
    );
};

const styles = StyleSheet.create({
    modal: {
        width: 300,
        backgroundColor: "#fff",
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    link: {
        color: "#5ab5e9",
        textDecorationLine: "underline",
        textDecorationStyle: "solid",
        fontSize: 20,
        marginTop: 5,
        marginBottom: 5,
    }
})

export default Modal;