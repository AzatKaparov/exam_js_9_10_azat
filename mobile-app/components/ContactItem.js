import React from 'react';
import {Avatar, ListItem} from "react-native-elements";

const ContactItem = ({img, name, phone, onClick}) => {
    return (
        <ListItem onPress={onClick} bottomDivider>
            <Avatar source={{uri: img}} />
            <ListItem.Content>
                <ListItem.Title>{name}</ListItem.Title>
                <ListItem.Subtitle>{phone}</ListItem.Subtitle>
            </ListItem.Content>
        </ListItem>
    );
};

export default ContactItem;