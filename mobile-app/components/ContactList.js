import React, {useEffect, useState} from 'react';
import {Text, View} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {choseContact, fetchContacts} from "../store/actions";
import ContactItem from "./ContactItem";
import Modal from "./Modal";

const ContactList = () => {
    const dispatch = useDispatch();
    const [visible, setVisible] = useState(true);
    const contacts = useSelector(state => state.contacts);

    useEffect(() => {
        dispatch(fetchContacts());
    }, []);

    return (
        <View>
            <Modal
                toggleOverlay={() => setVisible(false)}
                visible={visible}
            />
            {contacts.map(item => (
                <ContactItem
                    onClick={() => {
                        dispatch(choseContact(item))
                        setVisible(true);
                    }}
                    key={item.id}
                    img={item.photo}
                    name={item.name}
                    phone={item.phone}
                />
            ))}
        </View>
    );
};

export default ContactList;