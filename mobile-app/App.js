import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import thunk from 'redux-thunk';
import reducer from "./store/reducer";
import {applyMiddleware, createStore} from "redux";
import {Provider} from "react-redux";
import ContactList from "./components/ContactList";
import {Header} from "react-native-elements";


const store = createStore(
    reducer,
    applyMiddleware(thunk)
);

export default function App() {
  return (
    <Provider store={store}>
    <View style={styles.container}>
        <Header centerComponent={{ text: 'CONTACTS', style: { color: '#fff' } }}/>
        <ContactList/>
    </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
