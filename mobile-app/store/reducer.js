import {CHOSE_CONTACT, FETCH_CONTACTS_SUCCESS} from "./actions";

const initialState = {
    contacts: [],
    exact: {}
};

const parseData = obj => {
    return Object.keys(obj).map(key => {
        return {...obj[key], id: key}
    });
};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case CHOSE_CONTACT:
            return {...state, exact: action.contact};
        case FETCH_CONTACTS_SUCCESS:
            return {...state, contacts: parseData(action.contacts)};
        default:
            return state;
    }
}

export default reducer;