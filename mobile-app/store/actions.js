import axiosApi from "../axiosApi";

export const FETCH_CONTACTS_SUCCESS = "FETCH_CONTACTS_SUCCESS";
export const CHOSE_CONTACT = "CHOSE_CONTACT";

export const fetchContactsSuccess = contacts => ({type: FETCH_CONTACTS_SUCCESS, contacts: contacts});
export const choseContact = item => ({type: CHOSE_CONTACT, contact: item});

export const fetchContacts = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get('./contacts.json');
            dispatch(fetchContactsSuccess(response.data));
        } catch (e) {
            console.error(e)
        }
    };
};